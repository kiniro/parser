![npm bundle size (minified)](https://img.shields.io/bundlephobia/min/@kiniro/parser.svg)
![NpmLicense](https://img.shields.io/npm/l/@kiniro/parser.svg)

Parser for [@kiniro/lang](https://gitlab.com/kiniro/lang).

[gitlab](https://gitlab.com/kiniro/parser) / [npm](https://www.npmjs.com/package/@kiniro/parser)
