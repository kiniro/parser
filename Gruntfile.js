module.exports = function (grunt) {
    grunt.loadNpmTasks('grunt-mocha-test');
    grunt.loadNpmTasks('grunt-execute');

    grunt.initConfig({
        mochaTest: {
            options: { reporter: 'spec' },
            src: ['test/*.js']
        },
        execute: {
            parser: {
                options: {
                    args: ['--out', 'src/grammar.js', 'src/grammar.ne']
                },
                src: ['node_modules/nearley/bin/nearleyc.js']
            }
        },
    });

    grunt.registerTask('build', ['execute:parser']);
    grunt.registerTask('test', ['build', 'mochaTest']);
};
