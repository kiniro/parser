@{%
const lexer = require('./lexer');

const fix = x => x ? x : [];

const extract = numbers => x => {
    x = id(x);
    return numbers.map(n => x[n]);
};

const from1 = x => { x = id(x); return [x[0], ...x[1]]; }

%}

@lexer lexer

Par[X]                     -> %lparen  $X %rparen                           {% x => id(x[1]) %}

Brack[X]                   -> %lbrack $X %rbrack                            {% x => id(x[1]) %}

SepBy[Expr, Separator]     -> $Expr ($Separator $Expr):*                    {% x => [id(x[0])].concat(x[1].map(x => id(x[1])))  %}

exprs                      -> expr:+                                        {% id %}

expr                       -> Brack[expr:*]                                 {% x => ({ type: 'list', value: id(x) }) %}
                           | Par[SepBy[exprs, %dollar]]                     {% x => { x = id(x); let r = f = [];
                                                                                     for (var i = 0; i < x.length; i++) {
                                                                                         r.push(x[i]);
                                                                                         r = r[r.length - 1];
                                                                                     }
                                                                                     return f[0]; } %}
                           | Par[(%kw_begin exprs)]                         {% from1 %}

                           | Par[(%kw_lambda
                                  symbol:*
                                  %kw_arrow
                                  exprs)]
                                                                            {% x => { x = id(x); return[x[0], x[1], ...x[3]]; } %}

                           | Par[(%kw_let
                                  Par[(Par[(symbol expr)]):+]
                                  exprs)]
                                                                            {% x => { x = id(x);
                                                                                      return [x[0], x[1].map(id), ...x[2]]; } %}

                           # e.g. (set x (y z))
                           | Par[(%kw_set symbol expr)]                     {% extract([0, 1, 2]) %}

                           # e.g. (set obj "prop" value)
                           | Par[(%kw_set expr expr expr)]                  {% id %}

                           # e.g. (set obj.p1.p2 value)
                           | Par[(%kw_set expr %kw_dot symbol expr)]        {% x => { x = extract([0, 1, 3, 4])(x);
                                                                                      x[2].type = 'string';
                                                                                      return x; }  %}

                           # e.g. (if a b c)
                           | Par[(%kw_if expr expr expr)]                   {% extract([0, 1, 2, 3]) %}

                           # e.g. (def x (y z))
                           | Par[(%kw_def symbol expr)]                     {% extract([0, 1, 2]) %}

                           # e.g. (def f (x y z) (g x))
                           | Par[(%kw_def symbol Par[symbol:*] expr:+)]
                                                                            {% x => {
                                                                               x = id(x);
                                                                               return [x[0], x[1], x[2], ...x[3]];
                                                                            } %}

                           # e.g. (cond ((a b) ...))
                           | Par[(%kw_cond Par[(expr expr)]:+)]             {% from1 %}

                           | Par[(%kw_apply expr expr)]                     {% extract([0, 1, 2]) %}
                           | Par[(%kw_throw expr)]                          {% extract([0, 1]) %}
                           | Par[(%kw_try expr expr)]                       {% extract([0, 1, 2]) %}
                           | Par[(%kw_par exprs)]                           {% from1 %}
                           | Par[(%kw_async exprs)]                         {% from1 %}
                           | Par[(%kw_dot exprs)]                           {% from1 %}
                           | Par[(%kw_await expr)]                          {% extract([0, 1]) %}
                           | expr %kw_dot prop                              {% x => ({ type: 'prop', expr: x[0], prop: x[2] }) %}
                           | symbol                                         {% id %}
                           | %lfig (object_key %kw_colon expr
                                               %kw_comma:?):* %rfig         {% x => ({ type: 'object', props: x[1] }) %}
                           | %number                                        {% id %}
                           | %string                                        {% id %}

prop                       -> (symbol | %kw_begin | %kw_apply | %kw_def
                              | %kw_let | %kw_if | %kw_cond | %kw_throw
                              | %kw_try | %kw_set | %kw_par | %kw_async
                              | %kw_await)                                  {% x => { x = id(id(x));
                                                                                      x.type = 'symbol';
                                                                                      return x; } %}
symbol                     -> %symbol                                       {% id %}
object_key                 -> [a-zA-Z0-9_-]:+                               {% id %}