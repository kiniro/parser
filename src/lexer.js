const moo = require('moo');

const escapeSeqs = {
    '"': '"',
    '\\': '\\',
    'b': '\b',
    'f': '\f',
    'n': '\n',
    'r': '\r',
    't': '\t'
};

/** Escape string literal (same as 'eval') */
const escape = str => {
    let esc = false, buf = '';

    for (let i = 1, strlen = str.length - 1; i < strlen; i++) {
        let c = str[i];
        if (esc) {
            buf += escapeSeqs[c];
            esc = false;
        } else {
            if (c == '\\') {
                esc = true;
                continue;
            }
            buf += c;
        }
    }

    return buf;
};

const keywordList = ['begin', 'apply', 'def', 'let', 'if', 'cond', 'throw',
                     'try', 'set', 'par', 'async', 'await'];
const keywords = {};

keywordList.forEach(kw => {
    keywords['kw_'+kw] = kw;
});
keywords.kw_arrow = '->';

const lexer = moo.compile({
    comment1: /\/\/.*?$/,
    comment2: /;.*?$/,
    comment3: { match: /\/\*[^]*?\*\//, lineBreaks: true },
    number:  /[-+]?[0-9]*\.?[0-9]+/,
    symbol: {
        match: /[a-zA-Z!?+\->\/_=\*<][a-zA-Z0-9!?+\->\/\\_=\*<]*/,
        keywords
    },
    kw_colon: ':',
    kw_comma: ',',
    kw_dot: '.',
    kw_lambda: '\\',
    whitespace: { match: /[ \t\r\n]+/, lineBreaks: true },
    string:  { match: /"(?:\\["\\nbtrf]|[^\n"\\])*"/, value: escape },
    lparen:  '(',
    rparen:  ')',
    lbrack: '[',
    rbrack: ']',
    lfig: '{',
    rfig: '}',
    dollar: '$',
    error: moo.error,
});

lexer.next = (next => () => {
    let tok;
    while ((tok = next.call(lexer)) && (tok.type === "whitespace" ||
                                        tok.type === "comment1" ||
                                        tok.type === "comment2" ||
                                        tok.type === "comment3")) {}
    return tok;
})(lexer.next);

module.exports = lexer;
