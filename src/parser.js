const moo = require('moo');
const nearley = require("nearley");
const grammar = require("./grammar.js");

function parse (src) {
    const parser = new nearley.Parser(nearley.Grammar.fromCompiled(grammar));
    parser.feed(src);

    if (!parser.results.length) {
        throw "No parse";
    } else if (parser.results.length > 1) {
        throw "Ambiguous grammar";
    } else {
        return parser.results[0];
    }
}

const escapeSeqs = {
    '"': '"',
    '\\': '\\',
    'b': '\b',
    'f': '\f',
    'n': '\n',
    'r': '\r',
    't': '\t'
};

/** Escape string literal (same as 'eval') */
const escape = str => {
    let esc = false, buf = '';

    for (let i = 1, strlen = str.length - 1; i < strlen; i++) {
        let c = str[i];
        if (esc) {
            buf += escapeSeqs[c];
            esc = false;
        } else {
            if (c == '\\') {
                esc = true;
                continue;
            }
            buf += c;
        }
    }

    return buf;
};

const keywords = {};

['begin', 'apply', 'def', 'let', 'if', 'cond', 'throw',
 'try', 'set', 'par', 'async', 'await']
    .forEach(kw => {
        keywords['kw_'+kw] = kw;
    });

keywords.kw_arrow = '->';

const lexer = moo.compile({
    symbol: {
        match: /[a-zA-Z!?+\->\/_=\*<][a-zA-Z0-9!?+\->\/\\_=\*<]*/,
        keywords
    },
    kw_colon: ':',
    kw_comma: ',',
    kw_dot: '.',
    kw_lambda: '\\',
    whitespace: { match: /[ \t\r\n]+/, lineBreaks: true },
    comment: /;.*?$/,
    number:  /[0-9]*\.?[0-9]+/,
    string:  { match: /"(?:\\["\\nbtrf]|[^\n"\\])*"/, value: escape },
    lparen:  '(',
    rparen:  ')',
    lbrack: '[',
    rbrack: ']',
    lfig: '{',
    rfig: '}',
    dollar: '$',
    error: moo.error,
});

lexer.next = (next => () => {
    let tok;
    while ((tok = next.call(lexer)) && (tok.type === "whitespace" || tok.type === "comment")) {}
    return tok;
})(lexer.next);

module.exports = parse;
