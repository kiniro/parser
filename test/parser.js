const assert = require('assert');
const parse = require('../src/parser');

const traverse = x => {
    if (x instanceof Array) {
        return x.map(traverse);
    } else if (typeof x == 'undefined') {
        return '?';
    } else if (x.type === 'list') {
        return ['list', ...x.value.map(traverse)];
    } else {
        return x.value;
    }
};

const simplify = (ast, dropPositions = false) => {
    const simplify = ast => {
        if (ast instanceof Array) {
            return ast.map(simplify);
        } else {
            if (ast.value && ast.type) {
                if (dropPositions) {
                    with (ast) {
                        return { value, type };
                    }
                } else {
                    if (ast.line && ast.col) {
                        with (ast) {
                            return { value, type, line, col };
                        }
                    }
                }
            }
        }
        if (typeof ast == 'string') {
            return ast;
        }
        Object.keys(ast).forEach(key => {
            ast[key] = simplify(ast[key]);
        });
        return ast;
    };

    return simplify(ast);
};

const symbol = value => ({ type: 'symbol', value });

const prop = (expr, prop) => ({ type: 'prop', expr, prop });

const kw = name => ({ type: 'symbol', value: name });

const a = 'a';
const b = 'b';
const c = 'c';
const d = 'd';
const e = 'e';
const f = 'f';
const g = 'g';
const h = 'h';
const x = 'x';
const y = 'y';
const z = 'z';

describe('parser', () => {
    [
        ['(a b c)', [a, b, c]],
        ['( a b c )', [a, b, c]],
        ['(a b (c d))', [a, b, [c, d]]],
        ['(((a b)))', [[[a, b]]]],
        ['(if (a) (b) c)', ['if', [a], [b], c]],
        ['( if (a) (b) c )', ['if', [a], [b], c]],
        ['(begin (a) (b) c)', ['begin', [a], [b], c]],
        ['( begin (a) (b) c )', ['begin', [a], [b], c]],
        ['(\\ x y z -> a b c)', ['\\', [x, y, z], a, b, c]],
        ['( \\ x y z -> a b (c) )', ['\\', [x, y, z], a, b, [c]]],
        ['(let ((x y) (z (h))) (a) b)', ['let', [[x, y], [z, [h]]], [a], b]],
        ['(set x (y))', ['set', x, [y]]],
        ['(set a (+ b c))', ['set', a, ['+', b, c]]],
        ['( if (x) (y) (z) )', ['if', [x], [y], [z]]],
        ['(def f a)', ['def', f, a]],
        ['(def f (a))', ['def', f, [a]]],
        ['(def f (x y z) a)', ['def', f, [x, y, z], a]],
        ['(def f (x y z) (a b c))', ['def', f, [x, y, z], [a, b, c]]],
        ['(def f (x y z) (a b c) (d e f))', ['def', f, [x, y, z], [a, b, c],
                                             [d, e, f]
                                            ]],
        ['(def f () a)', ['def', 'f', [], a]],
        ['(def f () a (b))', ['def', 'f', [], a, [b]]],
        ['(cond (x y) ((f) (g)) (f (g)) ((f) g))', ['cond',
                                                    [x, y],
                                                    [[f], [g]],
                                                    [f, [g]],
                                                    [[f], g]]],
        ['(apply f g)', ['apply', 'f', 'g']],
        ['(apply (f) (g))', ['apply', ['f'], ['g']]],
        ['(sleep a (+ b c))', ['sleep', a, ['+', b, c]]],
        ['(throw (a b c))', ['throw', [a, b, c]]],
        ['(par (a b c) d (e f))', ['par', [a, b, c], d, ['e', 'f']]],
        ['(async (a b c) d (e f))', ['async', [a, b, c], d, ['e', 'f']]],
        ['(await (a b c))', ['await', [a, b, c]]],
        ['(+ 1 2)', ['+', '1', '2']],
        ['(a $ b)', [a, [b]]],
        ['(a $ b $ c $ d)', [a, [b, [c, [d]]]]],
        ['(a $ b $ (c d))', [a, [b, [[c, d]]]]],
        ['(a $ b $ (c $ d))', [a, [b, [[c, [d]]]]]],
        ['[a b c]', ['list', a, b, c]],
    ].forEach(([input, expected]) => {
        let res = parse(input);
        describe(input, () => {
            it('parses unambiguously', () => {
                assert.equal(res.length, 1);
            });
            it('parses correctly', () => {
                assert.deepStrictEqual(traverse(res[0]), expected, input);
            });
        });
    });

    describe('prop', () => {
        const a = symbol('a');
        const b = symbol('b');
        const c = symbol('c');
        const d = symbol('d');
        it('parses correctly', () => {
            assert.deepStrictEqual([
                'a',
                'a.b',
                'a.b.c',
                'a.b.c.d',
            ].map(input => simplify(parse(input)[0], true)), [
                a,
                prop(a, b),
                prop(prop(a, b), c),
                prop(prop(prop(a, b), c), d),
            ]);
        });

        describe('allows keywords as properties', () => {
            const mkAssertion = (input, expected) =>
                  assert.deepStrictEqual(simplify(parse(input)[0], true), expected);

            it('begin', () => {
                mkAssertion('a.begin', prop(a, kw('begin')));
            });

            it('apply', () => {
                mkAssertion('a.apply', prop(a, kw('apply')));
            });

            it('def', () => {
                mkAssertion('a.def', prop(a, kw('def')));
            });

            it('let', () => {
                mkAssertion('a.let', prop(a, kw('let')));
            });

            it('if', () => {
                mkAssertion('a.if', prop(a, kw('if')));
            });

            it('cond', () => {
                mkAssertion('a.cond', prop(a, kw('cond')));
            });

            it('throw', () => {
                mkAssertion('a.throw', prop(a, kw('throw')));
            });

            it('try', () => {
                mkAssertion('a.try', prop(a, kw('try')));
            });

            it('set', () => {
                mkAssertion('a.set', prop(a, kw('set')));
            });

            it('par', () => {
                mkAssertion('a.par', prop(a, kw('par')));
            });

            it('async', () => {
                mkAssertion('a.async', prop(a, kw('async')));
            });

            it('await', () => {
                mkAssertion('a.await', prop(a, kw('await')));
            });
        });

    });
});
