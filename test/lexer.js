const assert = require('assert');
const lexer = require('../src/lexer');

function tokenize (input) {
    const r = [];
    lexer.reset(input);
    while (true) {
        const next = lexer.next();
        if (typeof next === 'undefined') {
            break;
        }
        r.push(next);
    }
    return r;
};

describe('lexer', () => {
    [
        [ '())', ['lparen', 'rparen', 'rparen'] ],
        [ '[', ['lbrack'] ],
        [ ']', ['rbrack'] ],
        [ '(def set let foo)', ['lparen', 'kw_def', 'kw_set', 'kw_let',
                                'symbol', 'rparen'] ],
        [ '(def // x \nx 1)', ['lparen', 'kw_def',
                              'symbol', 'number', 'rparen'] ],
        [ '(\\ x -> y)', ['lparen', 'kw_lambda', 'symbol', 'kw_arrow', 'symbol', 'rparen']],
        [ '(x $ y $ z)', ['lparen', 'symbol', 'dollar', 'symbol', 'dollar', 'symbol', 'rparen']],
        [ '(x // comment \n y)', ['lparen', 'symbol', 'symbol', 'rparen']],
        [ '(x // comment /* */ \n y)', ['lparen', 'symbol', 'symbol', 'rparen']],
        [ '(x // comment // \n y)', ['lparen', 'symbol', 'symbol', 'rparen']],
        [ '(x // comment ;; \n y)', ['lparen', 'symbol', 'symbol', 'rparen']],
        [ '(x /* comment */ y)', ['lparen', 'symbol', 'symbol', 'rparen']],
        [ '(x /* \n \n \n * */ y)', ['lparen', 'symbol', 'symbol', 'rparen']],
        [ '(x /* \n \n \n /* */ y)', ['lparen', 'symbol', 'symbol', 'rparen']],
        [ '(x /* \n \n \n /* /* /* ;; // */ y)', ['lparen', 'symbol', 'symbol', 'rparen']],
        [ '(x /* \n */ */ y)', ['lparen', 'symbol',
                                /* this symbol stands for the last start+slash */
                                'symbol',
                                'symbol', 'rparen']],
        [ '1 2', ['number', 'number']],
        [ '1.1', ['number']],
        [ '0.1', ['number']],
        [ '-0.1', ['number']],
        [ '-10', ['number']],
        [ '""', ['string']],
        [ '"\\n"', ['string']],
        [ '"\\""', ['string']],
    ].forEach(([input, expected]) => {
        it(input, () => {
            assert.deepStrictEqual(tokenize(input)
                                   .map(token =>
                                        token.type), expected, input);
        });
    });
});
